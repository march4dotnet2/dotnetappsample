sonar.projectKey=march4dotnet2_dotnetappsample
sonar.organization=march4dotnet2

# This is the name and version displayed in the SonarCloud UI.
#sonar.projectName=dotnetappsample
#sonar.projectVersion=1.0

# Path is relative to the sonar-project.properties file. Replace "\" by "/" on Windows.
#sonar.sources=.

# Encoding of the source code. Default is default system encoding
#sonar.sourceEncoding=UTF-8